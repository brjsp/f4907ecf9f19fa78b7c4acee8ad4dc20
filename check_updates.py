#!/usr/bin/python3

import http.client, json, urllib.request

VERSIONS={
	'archlinux/arch-install-scripts': 'v26',
	'bitwarden/desktop': 'v2022.6.2',
	'bitwarden/clients': 'web-v2022.9.0',
	'discord': '0.0.19',
	'discord-ptb': '0.0.32',
	'discord-canary': '0.0.139',
	'discord-development': '0.0.203',
	'gyunaev/kchmviewer': 'RELEASE_8_0',
	'microsoft/vscode': '1.71.0',
	'moreutils': '0.67',
	'gede': '2.18.2',
	################
	'flash/activex':'34.0.0.267',
	'flash/npapi':'34.0.0.267',
	'flash/ppapi':'34.0.0.267',
	'flash/linux-64-rpm-ppapi':'34.0.0.137',
	'flash/linux-64-rpm-npapi':'34.0.0.137',
}

def expect(name, version):
	if(VERSIONS[name] != version):
		print(f'New version of {name}: {version}, we have {VERSIONS[name]}')

def discord_version(channel=None):
	conn = http.client.HTTPSConnection('discordapp.com')
	conn.request('HEAD', '/api/download'+(('/'+channel) if channel else '')+'?platform=linux&format=tar.gz')
	r1= conn.getresponse()
	expect('discord'+(('-'+channel) if channel else ''), r1.getheader('Location').split('/')[5])



def github_version(user,repo):
	with urllib.request.urlopen(f"https://api.github.com/repos/{user}/{repo}/tags") as url:
		data = json.loads(url.read().decode())
	expect(user+'/'+repo, data[0]['name'])

def github_version_releases(user,repo):
	conn = http.client.HTTPSConnection('github.com')
	conn.request('HEAD', f'/{user}/{repo}/releases/latest')
	r1= conn.getresponse()
	expect(user+'/'+repo, r1.getheader('Location').split('/')[7])



def flash_versions():
	with urllib.request.urlopen("https://api.flash.cn/config/flashVersion/") as url:
		data = json.loads(url.read().decode()[25:-2])
	for channel in {'activex','npapi','ppapi','linux-64-rpm-ppapi','linux-64-rpm-npapi'}:
		expect("flash/"+channel,data[channel]["version"])

def webscraper_version(name_, url_, prefix_, sufix_):
	with urllib.request.urlopen(url_) as url:
		data= url.read().decode()
	pos= data.find(prefix_)
	data= data[pos+len(prefix_):]
	pos= data.find(sufix_)
	data= data[:pos]
	expect(name_, data)


discord_version()
discord_version('ptb')
discord_version('canary')
discord_version('development')

github_version('bitwarden','desktop')
github_version_releases('bitwarden','clients')
github_version('archlinux','arch-install-scripts')
github_version_releases('microsoft','vscode')
#Kchmviewer has inane tagging scheme
github_version_releases('gyunaev','kchmviewer',)

webscraper_version('moreutils', "https://joeyh.name/code/moreutils/", 'news/version_', '/')
webscraper_version('gede', "https://gede.dexar.se/pmwiki.php?n=Downloads.Releases", "uploads/source/gede-", '.tar')

#not related
flash_versions()